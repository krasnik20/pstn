﻿using Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace StubCallsGenerator
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await Task.Delay(5000);
            var db = new Db();
            db.CallRecords.RemoveRange(db.CallRecords);
            var random = new Random();
            while (true)
            {
                await Task.Delay(random.Next(3000, 5000));
                var usersCount = db.Users.Count();
                var caller = db.Users.Skip(random.Next(usersCount == 0 ? 0 : usersCount - 1)).First();
                User recepient;
                do
                {
                    recepient = db.Users.Skip(random.Next(usersCount - 1)).First();
                } while (recepient.Id == caller.Id);
                var duration = TimeSpan.FromSeconds(random.Next(5, 2 * 60 * 60));
                db.Add(new CallRecord { Caller = caller, Recipient = recepient, Duration = duration, Start = DateTime.Now - duration });
                db.SaveChanges();
                Console.WriteLine("Call generated");
            }
        }
    }
}
