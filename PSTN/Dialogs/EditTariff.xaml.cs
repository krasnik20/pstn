﻿using Model;
using System.Windows;

namespace PSTN.Dialogs
{
    public partial class EditTariff : Window
    {
        public EditTariff(Tariff tariff)
        {
            InitializeComponent();
            DataContext = tariff;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
