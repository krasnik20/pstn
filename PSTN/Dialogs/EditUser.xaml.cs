﻿using Model;
using System.Linq;
using System.Windows;

namespace PSTN.Dialogs
{
    public partial class EditUser : Window
    {
        public EditUser(User user)
        {
            InitializeComponent();
            DataContext = user;
            Tariffs.ItemsSource = App.Db.Tariffs.ToList();
            Cities.ItemsSource = App.Db.Cities.ToList();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
