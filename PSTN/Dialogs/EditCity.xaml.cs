﻿using Model;
using System.Windows;

namespace PSTN.Dialogs
{
    public partial class EditCity : Window
    {
        public EditCity(City city)
        {
            InitializeComponent();
            DataContext = city;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
