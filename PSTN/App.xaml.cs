﻿using Model;
using System;
using System.Windows;

namespace PSTN
{
    public partial class App : Application
    {
        public static Db Db;
        public static CallsChecker CallsChecker;

        public App()
        {
            Db = new Db();
            CallsChecker = new CallsChecker();
            CallsChecker.Start(TimeSpan.FromSeconds(5));
        }
    }
}
