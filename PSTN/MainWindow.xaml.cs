﻿using Microsoft.EntityFrameworkCore;
using Model;
using PSTN.Dialogs;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PSTN
{
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            var Db = App.Db;

            //Db.Tariffs.Add(new Tariff { Name = "Дом+", InnerPrice = 0, OuterPrice = 5 });
            //Db.Cities.Add(new City { Name = "Москва" });
            //Db.Cities.Add(new City { Name = "Долгопрудный" });
            //Db.SaveChanges();

            //Db.Users.Add(new User { Name = "Павлов Владимир Анатольевич", City = Db.Cities.First(), Tariff = Db.Tariffs.First(), Balance = 442, ContractId = "12345678", PhoneNumber = 89996098602 });
            //Db.Users.Add(new User { Name = "Михайлов Валерий Станиславович", City = Db.Cities.First(), Tariff = Db.Tariffs.First(), Balance = 250, ContractId = "23456789", PhoneNumber = 89997891245 });
            //Db.Users.Add(new User { Name = "Власов Родион Кириллович", City = Db.Cities.Skip(1).First(), Tariff = Db.Tariffs.First(), Balance = 2, ContractId = "3456789a", PhoneNumber = 88005553535 });
            //Db.SaveChanges();

            UpdateCitiesItemsSource();
            UpdateTariffsItemsSource();
            UpdateUsersItemsSource();
            UpdateCallRecordsItemsSource();
        }

        private void AddOrEditUserClick(object sender, RoutedEventArgs e)
        {
            var user = FindBySender<User>(sender);
            var window = new EditUser(user);
            if (window.ShowDialog() == true)
            {
                EntitiesService.CreateOrUpdateItem(user);

                UpdateUsersItemsSource();
            }
        }

        private void AddOrEditTariffClick(object sender, RoutedEventArgs e)
        {
            var tariff = FindBySender<Tariff>(sender);
            var window = new EditTariff(tariff);
            if (window.ShowDialog() == true)
            {
                EntitiesService.CreateOrUpdateItem(tariff);

                UpdateTariffsItemsSource();
            }
        }

        private void AddOrEditCityClick(object sender, RoutedEventArgs e)
        {

            var City = FindBySender<City>(sender);
            var window = new EditCity(City);
            if (window.ShowDialog() == true)
            {
                EntitiesService.CreateOrUpdateItem(City);

                UpdateCitiesItemsSource();
            }
        }

        private void RemoveCityClick(object sender, RoutedEventArgs e)
        {
            EntitiesService.RemoveItem(FindBySender<City>(sender));

            UpdateCitiesItemsSource();
        }

        private void RemoveUserClick(object sender, RoutedEventArgs e)
        {
            EntitiesService.RemoveItem(FindBySender<User>(sender));

            UpdateUsersItemsSource();
        }

        private void RemoveTariffClick(object sender, RoutedEventArgs e)
        {
            EntitiesService.RemoveItem(FindBySender<Tariff>(sender));

            UpdateTariffsItemsSource();
        }

        private T FindBySender<T>(object sender) where T : Entity, new()
        {
            var id = (int)((sender as Button).Tag ?? 0);
            return id == 0 ? new T() : App.Db.Set<T>().FirstOrDefault(s => s.Id == id);
        }

        private void UpdateUsersItemsSource() => Users.ItemsSource = App.Db.Users.AsNoTracking().Include(u => u.Tariff).Include(u => u.City).ToList();

        private void UpdateTariffsItemsSource() => Tariffs.ItemsSource = App.Db.Tariffs.ToList();

        private void UpdateCitiesItemsSource() => Cities.ItemsSource = App.Db.Cities.ToList();

        private void UpdateCallRecordsItemsSource() => Calls.ItemsSource = App.Db.CallRecords.AsNoTracking().Include(cr => cr.Caller.City).Include(cr => cr.Recipient.City).OrderByDescending(c => c.Id).ToList();

        private void RefreshCallsButton_Click(object sender, RoutedEventArgs e) => UpdateCallRecordsItemsSource();
        private void RefreshUsersButton_Click(object sender, RoutedEventArgs e) => UpdateUsersItemsSource();

        private void Tabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CallsTab.IsSelected)
                UpdateCallRecordsItemsSource();

            if (UsersTab.IsSelected)
                UpdateUsersItemsSource();
        }
    }
}
