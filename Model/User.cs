﻿namespace Model
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string ContractId { get; set; }
        public ulong PhoneNumber { get; set; }
        public double Balance { get; set; }
        public Tariff Tariff { get; set; }
        public City City { get; set; }
    }
}
