﻿using Microsoft.EntityFrameworkCore;

namespace Model
{
    public class Db : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<CallRecord> CallRecords { get; set; }

        public Db()
        {
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=CallsDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasMany<User>().WithOne(cr => cr.City).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Tariff>().HasMany<User>().WithOne(cr => cr.Tariff).OnDelete(DeleteBehavior.SetNull);
        }
    }
}
