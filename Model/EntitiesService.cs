﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Model
{
    public class EntitiesService
    {
        public static void CreateOrUpdateItem<T>(T entity) where T : Entity
        {
            var db = new Db();

            if (entity.Id == 0)
                db.Attach(entity);
            else
                db.Update(entity);

            var references = db.Entry(entity).References;

            foreach (var element in references)
            {
                if (element.CurrentValue == null)
                    continue;

                if (db.Entry(element.CurrentValue).State == EntityState.Unchanged)
                    db.Entry(element.CurrentValue).State = EntityState.Detached;
            }


            db.SaveChanges();
        }

        public static void RemoveItem<T>(T entity) where T : Entity
        {
            var db = new Db();

            if(entity is User user)
            {
                db.CallRecords.RemoveRange(db.CallRecords.Where(cr => cr.Caller.Id == user.Id || cr.Recipient.Id == user.Id));
            }

            db.Set<T>().Remove(entity);
            db.SaveChanges();
        }
    }
}
