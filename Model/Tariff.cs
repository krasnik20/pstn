﻿namespace Model
{
    public class Tariff : Entity
    {
        public string Name { get; set; }
        public double InnerPrice { get; set; }
        public double OuterPrice { get; set; }
        public string Description => $"Звонки внутри города - {InnerPrice} руб./мин.\nМеждугородние звонки {OuterPrice} руб./мин.";
    }
}
