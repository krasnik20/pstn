﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class CallsChecker
    {
        private System.Timers.Timer timer;

        public void Stop()
        {
            timer?.Stop();
        }

        public void Start(TimeSpan period)
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.Elapsed += (s, e) =>
                {
                    var db = new Db();
                    var query = db.CallRecords
                        .Include(c => c.Caller.Tariff)
                        .Include(c => c.Caller.City)
                        .Include(c => c.Recipient.City);
                    var result = query.Where(c => c.Payment == null).ToList();

                    SetPayments(result);

                    db.UpdateRange(result);

                    var checks = result.Select(r => new { Caller = r.Caller, Payment = r.Payment });

                    foreach (var check in checks)
                        check.Caller.Balance -= check.Payment.Value;

                    db.UpdateRange(checks.Select(c => c.Caller));

                    db.SaveChanges();

                };
            }
            timer.Interval = period.TotalMilliseconds;
            timer.Start();
        }

        public static double CalculatePayment(CallRecord record)
        {
            if(record == null)
                throw new ArgumentException();

            return Math.Ceiling(record.Duration.TotalMinutes) * GetPrice(record);
        }

        public static double GetPrice(CallRecord record)
        {
            if (record == null || record.Caller == null || record.Caller.City == null || record.Caller.Tariff == null || record.Recipient == null || record.Recipient.City == null)
                throw new ArgumentException();

            return record.Caller.City.Id == record.Recipient.City.Id ? record.Caller.Tariff.InnerPrice : record.Caller.Tariff.OuterPrice;
        }

        public static void SetPayments(IEnumerable<CallRecord> records)
        {
            foreach (var record in records)
                record.Payment = CalculatePayment(record);
        }
    }
}
