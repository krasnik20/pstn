﻿using System;

namespace Model
{
    public class CallRecord : Entity
    {
        public User Caller { get; set; }
        public User Recipient { get; set; }
        public TimeSpan Duration { get; set; }
        public DateTime Start { get; set; }
        public double? Payment { get; set; }
    }
}
