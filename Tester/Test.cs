using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using PSTN.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Tester
{
[TestClass]
public class Test
{
    private City city1;
    private City city2;
    private Tariff tariff;
    private User user1;
    private User user2;
    private User user3;
    private CallRecord callRecord1;
    private CallRecord callRecord2;
    private CallRecord callRecordWithoutUsers;
    private User userWithoutCity;
    private CallRecord callRecordWithoutCity;
    private User userWithoutTariff;
    private CallRecord callRecordWithoutTariff;

    [TestMethod]
    public void TestCallsCalculator()
    {
        InitData();

        var expectedValue1 = 6d;
        var expectedValue2 = 4d;
        var expectedValue3 = 12d;
        var expectedValue4 = 16d;

        Assert.AreEqual(expectedValue1, CallsChecker.GetPrice(callRecord1));
        Assert.AreEqual(expectedValue2, CallsChecker.GetPrice(callRecord2));
        Assert.AreEqual(expectedValue3, CallsChecker.CalculatePayment(callRecord1));
        Assert.AreEqual(expectedValue4, CallsChecker.CalculatePayment(callRecord2));
        Assert.ThrowsException<ArgumentException>(() => 
            CallsChecker.GetPrice(callRecordWithoutUsers));
        Assert.ThrowsException<ArgumentException>(() => 
            CallsChecker.GetPrice(callRecordWithoutCity));
        Assert.ThrowsException<ArgumentException>(() => 
            CallsChecker.GetPrice(callRecordWithoutTariff));

        var records = new List<CallRecord> { callRecord1, callRecord2 };

        CallsChecker.SetPayments(records);

        var result = records.Select(r => r.Payment).ToList();
        var expectedResult = new List<double> { expectedValue3, expectedValue4 };

        for (int i = 0; i < result.Count; i++)
            Assert.AreEqual(result[i], expectedResult[i]);
    }

    [TestMethod]
    public void TestAddUser()
    {
        InitData();

        user1.Id = 0;
        user1.Tariff = null;
        user1.City = null;

        TestCreateItem(user1, nameof(user1.Name));
    }

    [TestMethod]
    public void TestAddCity()
    {
        InitData();

        city1.Id = 0;

        TestCreateItem(city1, nameof(city1.Name));
    }

    [TestMethod]
    public void TestAddTariff()
    {
        InitData();

        tariff.Id = 0;

        TestCreateItem(tariff, nameof(tariff.Name));
    }

    [TestMethod]
    public void TestEditUser() => TestUpdateItem<User>(nameof(User.Name));

    [TestMethod]
    public void TestEditCity() => TestUpdateItem<City>(nameof(City.Name));

    [TestMethod]
    public void TestEditTariff() => TestUpdateItem<Tariff>(nameof(City.Name));

    [TestMethod]
    public void TestRemoveUser() => TestRemoveItem<User>();

    [TestMethod]
    public void TestRemoveCity() => TestRemoveItem<City>();

    [TestMethod]
    public void TestRemoveTariff() => TestRemoveItem<Tariff>();

    private void TestUpdateItem<T>(string updatableStringPropertyName) where T : Entity
    {
        var db = new Db();

        var item = db.Set<T>().AsNoTracking().FirstOrDefault();
        Assert.IsNotNull(item);
            
        var testValue = "testValue";
        var property = item.GetType().GetProperty(updatableStringPropertyName);
        var originalValue = property.GetValue(item);
        Assert.AreNotEqual(originalValue, testValue);
        property.SetValue(item, testValue);
        EntitiesService.CreateOrUpdateItem(item);
        var updatedItem = db.Set<T>().FirstOrDefault();
        Assert.AreEqual(property.GetValue(item), property.GetValue(updatedItem));
        property.SetValue(updatedItem, originalValue);
        EntitiesService.CreateOrUpdateItem(updatedItem);
    }

    private void TestCreateItem<T>(T item, string testPropertyName) where T : Entity
    {
        var db = new Db();

        var property = item.GetType().GetProperty(testPropertyName);
        EntitiesService.CreateOrUpdateItem(item);
        var createdItem = db.Set<T>().FirstOrDefault(i => i.Id == item.Id);
        Assert.IsNotNull(createdItem);
        Assert.AreEqual(property.GetValue(item), property.GetValue(createdItem));
    }

    private void TestRemoveItem<T>() where T : Entity
    {
        var db = new Db();

        var item = db.Set<T>().AsNoTracking().FirstOrDefault();
        Assert.IsNotNull(item);
        var id = item.Id;

        EntitiesService.RemoveItem(item);
        var removedItem = db.Set<T>().FirstOrDefault(i => i.Id == id);
        Assert.IsNull(removedItem);
    }

    private void InitData()
    {
        city1 = new City { Id = 5, Name = "Test" };
        city2 = new City { Id = 7, Name = "Test" };
        tariff = new Tariff { Id = 23, Name = "Test", InnerPrice = 4, OuterPrice = 6 };
        user1 = new User { Id = 8, Name = "Test", City = city1, Tariff = tariff };
        user2 = new User { Id = 9, Name = "Test", City = city2, Tariff = tariff };
        user3 = new User { Id = 10, Name = "Test", City = city2, Tariff = tariff };
        callRecord1 = new CallRecord 
        { 
            Caller = user1, 
            Recipient = user2, 
            Duration = TimeSpan.FromSeconds(72) 
        };
        callRecord2 = new CallRecord 
        { 
            Caller = user3, 
            Recipient = user2, 
            Duration = TimeSpan.FromSeconds(185) 
        };

        callRecordWithoutUsers = new CallRecord 
        {
            Id = 1, 
            Duration = TimeSpan.FromSeconds(72), 
            Payment = null, 
            Start = DateTime.Now 
        };
        userWithoutCity = new User 
        { 
            Id = 2, 
            Name = "Test", 
            Balance = 25, 
            Tariff = tariff 
        };
        callRecordWithoutCity = new CallRecord 
        { 
            Id = 1, 
            Duration = TimeSpan.FromSeconds(72), 
            Payment = null, 
            Start = DateTime.Now, 
            Caller = userWithoutCity, 
            Recipient = user1 
        };
        userWithoutTariff = new User { Id = 2, Name = "Test", Balance = 25, City = city1 };
        callRecordWithoutTariff = new CallRecord 
        {
            Id = 1, 
            Duration = TimeSpan.FromSeconds(72), 
            Payment = null, 
            Start = DateTime.Now, 
            Caller = userWithoutTariff, 
            Recipient = user1 
        };
    }
}
}
